#! /bin/bash

KERNEL_PATH="/home/ksy/kernels"

#KERNEL_DIR_PATH="${KERNEL_PATH}/printk_aios"
#KERNEL_DIR_PATH="${KERNEL_PATH}/wqueue_btrfs_aios"
KERNEL_DIR_PATH="${KERNEL_PATH}/multithread_btrfs_aios"
#KERNEL_DIR_PATH="${KERNEL_PATH}/ksy_btrfs_aios"
#KERNEL_DIR_PATH="${KERNEL_PATH}/zios"
#KERNEL_DIR_PATH="${KERNEL_PATH}/xios"

cd ${KERNEL_DIR_PATH}

make -j `grep -c processor /proc/cpuinfo` && make modules && make modules_install && make install && reboot
