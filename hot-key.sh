#! /bin/bash

echo -e "\n####### Hot Key #######\n"

echo "m : Mount Btrfs directory"
echo "f : Do fio test"
echo "c : Compile kernel"

echo
read -p "Selection: (quit) " hot_key
echo

cd /home/ksy/scripts
hot_key=${hot_key:-q}
if [ ${hot_key} == m ]; then
    ./mount.sh
elif [ ${hot_key} == f ]; then
    ../fio/do_fio.sh
elif [ ${hot_key} == c ]; then
    ./compile.sh
fi

echo
