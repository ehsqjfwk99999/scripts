#! /bin/bash

DEVICE_PATH="/dev/nvme0n1"
MOUNT_PATH="/mnt/btrfs_dir"

# Check if root.
read -p "Login as root (yes)? " input
if [ ${input:-yes} == n ] || [ ${input:-yes} == no ]; then
    echo -e "Login as root first ... Exit\n"
    exit
fi

# Check device path.
echo -n "Checking device path ... "
if [ -b ${DEVICE_PATH} ]; then
    echo "Done"
else
    echo -e "Device not found ... Exit\n"
	exit
fi

# mkfs.
echo -ne "\nExecuting mkfs ... "
mkfs.btrfs -f "${DEVICE_PATH}" &> /dev/null
if [ ${?} -eq 0 ]; then
    echo "Done"
else
    echo -e "Error ... Exit\n"
	exit
fi

# Check mount path.
echo -n "Checking mount path ... "
if [ -d ${MOUNT_PATH} ]; then
    echo "Done"
else
    echo -n "Creating ... "
    mkdir ${MOUNT_PATH}
    echo "Done"
fi

# mount.
echo -n "Executing mount ... "
# mount "${DEVICE_PATH}" "${MOUNT_PATH}" &> /dev/null
mount -o commit=10 "${DEVICE_PATH}" "${MOUNT_PATH}" &> /dev/null
# mount -o notreelog,commit=10 "${DEVICE_PATH}" "${MOUNT_PATH}" &> /dev/null
if [ ${?} -eq 0 ]; then
    echo "Done"
else
    echo -e "Error ... Exit\n"
	exit
fi

echo -e "\nMount Complete!\n"
